#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <set>
using std::set;

/* TODO: write a function to compute all *k-subsets* of a given set.
 * Recall that k-subsets are subsets with *precisely* k elements.
 * Below are some function prototypes you could use.  See the notes
 * for the intuition and an outline. */

/* with sets: */
set<set<int> > ksubsets(set<int>& S, size_t k)
{
	if (k== 0) {
		set<int> empty;
		set<set<int> > P;
		P.insert(empty);
		return P;
	}

	if (S.size() < k) {
		set<set<int> > P;
		return P; 
	}

	int leftout = *S.begin(); 
	S.erase(leftout); 

	set<set<int> > T = ksubsets(S, k);

	set<set<int> > P(T); 

	set<set<int> > C = ksubsets(S, k-1); 

	for (set<set<int> >::iterator i = C.begin(); i!=C.end(); i++) {
		set<int> fuk = *i;
		fuk.insert(leftout);
		P.insert(fuk); 
	}
	S.insert(leftout);
	return P;
}

/* or with vectors: */
vector<vector<int> > ksubsets(vector<int>& V, size_t k);

int main()
{
	/* TODO: write some test code. */

	set<int> X = {1, 2, 3};
	set<set<int> > P = ksubsets(X, 2); 
	cout << "{\n";
	for (set<set<int> >::iterator i = P.begin(); i!=P.end(); i++) 
	{
		cout << "   {";
		for (set<int>::iterator j = (*i).begin(); j!=(*i).end(); j++) {
			cout << *j << " ";
		}
		cout << "},\n";
	}
	cout << "}\n";
	return 0;
}
